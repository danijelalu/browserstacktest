package utils;

public class Product {

    private String name;
    private double price;

    public String getName() {
        return name;
    }

    public void setName(String size) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
