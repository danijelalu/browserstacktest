package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class MyDriver {

    public static WebDriver driver;

        public static final String USERNAME = "danijelalalovic1";
        public static final String AUTOMATE_KEY = "ZpVJxA8pvnqWazQMGEzQ";
        public static final String BS_URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

        private MyDriver() {}

    public static WebDriver getDriver() {

        if (driver == null) {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", "Chrome");
            caps.setCapability("browser_version", "79.0");
            caps.setCapability("os", "Windows");
            caps.setCapability("os_version", "10");
            caps.setCapability("resolution", "1280x1024");
            caps.setCapability("name", "Bstack-[Java] Sample Test");

            try {
                driver = new RemoteWebDriver(new URL(BS_URL), caps);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        return driver;
    }

    public static void closeDriver() {
        driver.navigate().to("about:blank");
        driver.manage().deleteAllCookies();
        driver.getWindowHandles().clear();
    }

    public static void quitDriver() {
        driver.quit();
        driver = null;
    }
}
