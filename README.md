Postupak za dodavanje BStack

1. napraviti novi paket pod nazivom driver
2. napraviti u okviru njega novu klasu MyDriver
3. podesiti password i username u skladu sa konfiguracijom koja je oznacena na BStack
4. U svaku klasu u paketima ( pages, components, i test importovati klasu MyDriver)
5. Ako su testovi imali vec base klasu, nju skloniti.